// Copyright 2025 Ryan Curtin (http://www.ratml.org/)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <bandicoot>
#include "catch.hpp"

using namespace coot;

// Make sure .each_row(x) on an empty matrix does nothing.
TEMPLATE_TEST_CASE("each2_row_empty", "[each2]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // size is 0x0
  Mat<eT> x;

  x.each_row(uvec()) = zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_row(uvec()) += zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_row(uvec()) -= zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_row(uvec()) %= zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_row(uvec()) /= zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );
  }



// Make sure .each_col(x) on an empty matrix does nothing.
TEMPLATE_TEST_CASE("each2_col_empty", "[each2]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x;

  x.each_col(uvec()) = zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_col(uvec()) += zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_col(uvec()) -= zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_col(uvec()) %= zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_col(uvec()) /= zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );
  }



// Make sure that .each_row() on a one-row matrix is the same as just modifying the matrix.
TEMPLATE_TEST_CASE("each2_row_one_row", "[each2]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(1, 100, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 50));
  Mat<eT> orig_x(x);
  Mat<eT> y = x;
  uvec i(1, fill::zeros);

  x.each_row(i) = v;
  y = v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(i) += v;
  y += v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(i) -= v;
  y -= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(i) %= v;
  y %= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(i) /= v;
  y /= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col(x) on a one-col matrix is the same as just modifying the matrix.
TEMPLATE_TEST_CASE("each2_col_one_col", "[each2]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(50, 1, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(50, distr_param(1, 50));
  Mat<eT> orig_x(x);
  Mat<eT> y = x;
  uvec i(1, fill::zeros);

  x.each_col(i) = v;
  y = v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(i) += v;
  y += v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(i) -= v;
  y -= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(i) %= v;
  y %= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(i) /= v;
  y /= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row(x) works correctly on a full matrix.
TEMPLATE_TEST_CASE("each2_row_full_mat", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(120, 100, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 10));
  uvec rows = shuffle(linspace<uvec>(0, 119, 120));
  rows = rows.subvec(0, 30);

  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_row(rows) = v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) = v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) += v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) += v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) -= v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) -= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) %= v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) %= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) /= v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) /= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col(x) works correctly on a full matrix.
TEMPLATE_TEST_CASE("each2_col_full_mat", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 10));
  uvec cols = shuffle(linspace<uvec>(0, 119, 120));
  cols = cols.subvec(0, 30);

  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_col(cols) = v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) = v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) += v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) += v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) -= v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) -= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) %= v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) %= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) /= v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) /= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row(x) works correctly on an expression.
TEMPLATE_TEST_CASE("each2_row_full_mat_expr", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(120, 100, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 10));
  uvec rows = shuffle(linspace<uvec>(0, 119, 120));
  rows = rows.subvec(0, 50);

  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_row(rows) = (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) = (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) += (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) += (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) -= (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) -= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) %= (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) %= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) /= (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) /= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col(x) works correctly on an expression.
TEMPLATE_TEST_CASE("each2_col_full_mat_expr", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 10));
  uvec cols = shuffle(linspace<uvec>(0, 119, 120));
  cols = cols.subvec(0, 50);

  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_col(cols) = (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) = (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) += (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) += (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) -= (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) -= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) %= (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) %= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) /= (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) /= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row(x) on an alias works properly.
TEMPLATE_TEST_CASE("each2_row_full_mat_alias", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  x.row(0) = randi<Row<eT>>(120, distr_param(1, 20));
  uvec rows = shuffle(linspace<uvec>(0, 99, 100));
  rows = rows.subvec(0, 4);
  Mat<eT> orig_x(x);
  Mat<eT> y(x);

  x.each_row(rows) = x.row(0);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) = orig_x.row(0);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) += x.row(0);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) += orig_x.row(0);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) -= x.row(0);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) -= orig_x.row(0);

  // Add 1 to each element to avoid 0s in the matrices being compared
  REQUIRE( approx_equal(x + 1, y + 1, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) %= x.row(0);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) %= orig_x.row(0);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) /= x.row(0);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) /= orig_x.row(0);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col(x) on an alias works properly.
TEMPLATE_TEST_CASE("each2_col_full_mat_alias", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(100, 100, distr_param(1, 50)) + 50;
  x.row(0) = randi<Row<eT>>(100, distr_param(1, 20)) + 6;
  x(0, 0) = 5;
  uvec cols = shuffle(linspace<uvec>(0, 99, 100));
  cols = cols.subvec(0, 50);
  Mat<eT> orig_x(x);
  Mat<eT> y(x);

  // a little trickier expression here
  x.each_col(cols) = x.row(0).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) = orig_x.row(0).t();

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) += x.row(0).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) += orig_x.row(0).t();

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) -= x.row(0).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) -= orig_x.row(0).t();

  // The +1 handles any zero-valued elements.
  REQUIRE( approx_equal(x + 1, y + 1, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) %= x.row(0).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) %= orig_x.row(0).t();

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) /= x.row(0).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) /= orig_x.row(0).t();

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() with a conv_to as the RHS does the conversion implicitly.
TEMPLATE_TEST_CASE
  (
  "each2_row_conv_to",
  "[each2]",
  (std::pair<double, double>), (std::pair<double, float>), (std::pair<double, u32>), (std::pair<double, s32>), (std::pair<double, u64>), (std::pair<double, s64>),
  (std::pair<float, float>), (std::pair<float, double>), (std::pair<float, u32>), (std::pair<float, s32>), (std::pair<float, u64>), (std::pair<float, s64>),
  (std::pair<u32, u32>), (std::pair<u32, double>), (std::pair<u32, float>), (std::pair<u32, s32>), (std::pair<u32, u64>), (std::pair<u32, s64>),
  (std::pair<s32, s32>), (std::pair<s32, double>), (std::pair<s32, float>), (std::pair<s32, u32>), (std::pair<s32, u64>), (std::pair<s32, s64>),
  (std::pair<u64, u64>), (std::pair<u64, double>), (std::pair<u64, float>), (std::pair<u64, u32>), (std::pair<u64, s32>), (std::pair<u64, s64>),
  (std::pair<s64, s64>), (std::pair<s64, double>), (std::pair<s64, float>), (std::pair<s64, u32>), (std::pair<s64, s32>), (std::pair<s64, u64>)
  )
  {
  typedef typename TestType::first_type eT1;
  typedef typename TestType::second_type eT2;

  if (!coot_rt_t::is_supported_type<eT1>() || !coot_rt_t::is_supported_type<eT2>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT1> x = 10 * randi<Mat<eT1>>(100, 120, distr_param(1, 50)) + 50;
  Row<eT2> v = randi<Row<eT2>>(120, distr_param(1, 50));
  Row<eT1> conv_v = conv_to<Row<eT1>>::from(v);
  uvec rows = shuffle(linspace<uvec>(0, 99, 100));
  rows = rows.subvec(0, 50);

  Mat<eT1> orig_x(x);
  Mat<eT1> y(x);

  x.each_row(rows) = conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) = conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) += conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) += conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) -= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) -= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) %= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) %= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row(rows) /= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) /= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() with a conv_to as the RHS does the conversion implicitly.
TEMPLATE_TEST_CASE
  (
  "each2_col_conv_to",
  "[each2]",
  (std::pair<double, double>), (std::pair<double, float>), (std::pair<double, u32>), (std::pair<double, s32>), (std::pair<double, u64>), (std::pair<double, s64>),
  (std::pair<float, float>), (std::pair<float, double>), (std::pair<float, u32>), (std::pair<float, s32>), (std::pair<float, u64>), (std::pair<float, s64>),
  (std::pair<u32, u32>), (std::pair<u32, double>), (std::pair<u32, float>), (std::pair<u32, s32>), (std::pair<u32, u64>), (std::pair<u32, s64>),
  (std::pair<s32, s32>), (std::pair<s32, double>), (std::pair<s32, float>), (std::pair<s32, u32>), (std::pair<s32, u64>), (std::pair<s32, s64>),
  (std::pair<u64, u64>), (std::pair<u64, double>), (std::pair<u64, float>), (std::pair<u64, u32>), (std::pair<u64, s32>), (std::pair<u64, s64>),
  (std::pair<s64, s64>), (std::pair<s64, double>), (std::pair<s64, float>), (std::pair<s64, u32>), (std::pair<s64, s32>), (std::pair<s64, u64>)
  )
  {
  typedef typename TestType::first_type eT1;
  typedef typename TestType::second_type eT2;

  if (!coot_rt_t::is_supported_type<eT1>() || !coot_rt_t::is_supported_type<eT2>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT1> x = 10 * randi<Mat<eT1>>(120, 100, distr_param(1, 50)) + 50;
  Col<eT2> v = randi<Col<eT2>>(120, distr_param(1, 50));
  Col<eT1> conv_v = conv_to<Col<eT1>>::from(v);
  uvec cols = shuffle(linspace<uvec>(0, 99, 100));
  cols = cols.subvec(0, 50);

  Mat<eT1> orig_x(x);
  Mat<eT1> y(x);

  x.each_col(cols) = conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) = conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) += conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) += conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) -= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) -= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) %= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) %= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col(cols) /= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) /= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



//
// subviews
//



// Make sure that .each_row(x) on a one-row subview is the same as just modifying the matrix.
TEMPLATE_TEST_CASE("each2_row_subview_one_row", "[each2]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(3, 150, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 50));
  uvec r(1, fill::zeros);
  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(0, 0, 0, 99);
  Mat<eT> orig_y(y);

  x.submat(0, 0, 0, 99).each_row(r) = v;
  y = v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 0, 99).each_row(r) += v;
  y += v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 0, 99).each_row(r) -= v;
  y -= v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 0, 99).each_row(r) %= v;
  y %= v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 0, 99).each_row(r) /= v;
  y /= v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col(x) on a one-col subview is the same as just modifying the matrix.
TEMPLATE_TEST_CASE("each2_col_subview_one_col", "[each2]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(150, 3, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 50));
  uvec c(1, fill::zeros);
  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(0, 1, 99, 1);
  Mat<eT> orig_y(y);

  x.submat(0, 1, 99, 1).each_col(c) = v;
  y = v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 1, 99, 1).each_col(c) += v;
  y += v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 1, 99, 1).each_col(c) -= v;
  y -= v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 1, 99, 1).each_col(c) %= v;
  y %= v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 1, 99, 1).each_col(c) /= v;
  y /= v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row(x) on a subview works correctly for more than one row.
TEMPLATE_TEST_CASE("each2_row_subview_full_mat", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(220, 200, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 10));
  uvec rows = shuffle(linspace<uvec>(0, 119, 120));
  rows = rows.subvec(0, 6);

  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(10, 10, 129, 109);
  Mat<eT> orig_y(y);

  x.submat(10, 10, 129, 109).each_row(rows) = v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) = v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 129, 109).each_row(rows) += v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) += v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 129, 109).each_row(rows) -= v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) -= v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 129, 109).each_row(rows) %= v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) %= v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 129, 109).each_row(rows) /= v;
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) /= v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col(x) on a subview works correctly for more than one row.
TEMPLATE_TEST_CASE("each2_col_subview_full_mat", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 10));
  uvec cols = shuffle(linspace<uvec>(0, 119, 120));
  cols = cols.subvec(0, 75);

  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(15, 15, 114, 134);
  Mat<eT> orig_y(y);

  x.submat(15, 15, 114, 134).each_col(cols) = v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) = v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 114, 134).each_col(cols) += v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) += v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 114, 134).each_col(cols) -= v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) -= v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 114, 134).each_col(cols) %= v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) %= v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 114, 134).each_col(cols) /= v;
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) /= v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row(x) on a subview works correctly on an expression.
TEMPLATE_TEST_CASE("each2_row_subview_full_mat_expr", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(220, 200, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 10));
  uvec rows = shuffle(linspace<uvec>(0, 119, 120));
  rows = rows.subvec(0, 50);

  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(15, 15, 134, 114);
  Mat<eT> orig_y(y);

  x.submat(15, 15, 134, 114).each_row(rows) = (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) = (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 134, 114).each_row(rows) += (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) += (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 134, 114).each_row(rows) -= (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) -= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 134, 114).each_row(rows) %= (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) %= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 134, 114).each_row(rows) /= (2 * v.t() + 3);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) /= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col(x) on a subview works correctly on an expression.
TEMPLATE_TEST_CASE("each2_col_subview_full_mat_expr", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 10));
  uvec cols = shuffle(linspace<uvec>(0, 119, 120));
  cols = cols.subvec(0, 50);

  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(10, 10, 109, 129);
  Mat<eT> orig_y(y);

  x.submat(10, 10, 109, 129).each_col(cols) = (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) = (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 129).each_col(cols) += (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) += (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 129).each_col(cols) -= (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) -= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 129).each_col(cols) %= (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) %= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 129).each_col(cols) /= (2 * v.t() + 3);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) /= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row(x) on a subview with an alias as input works properly.
TEMPLATE_TEST_CASE("each2_row_subview_full_mat_alias", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 120;
  x.submat(0, 10, 0, 129) = 120 - linspace<Row<eT>>(0, 119, 120);
  Mat<eT> orig_x(x);
  Mat<eT> y(x.submat(0, 0, 99, 119));
  Mat<eT> orig_y(y);
  uvec rows = shuffle(linspace<uvec>(0, 99, 100));
  rows = rows.subvec(0, 50);

  x.submat(0, 0, 99, 119).each_row(rows) = x.submat(0, 10, 0, 129);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) = orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 99, 119).each_row(rows) += x.submat(0, 10, 0, 129);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) += orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 99, 119).each_row(rows) -= x.submat(0, 10, 0, 129);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) -= orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119) + 1, y + 1, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 99, 119).each_row(rows) %= x.submat(0, 10, 0, 129);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) %= orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 99, 119).each_row(rows) /= x.submat(0, 10, 0, 129);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) /= orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119) + 1, y + 1, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col(x) on a subview with an alias as input works properly.
TEMPLATE_TEST_CASE("each2_col_subview_full_mat_alias", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(200, 200, distr_param(1, 50)) + 50;
  x.submat(0, 0, 0, 99) = randi<Row<eT>>(100, distr_param(1, 20));
  Mat<eT> orig_x(x);
  Mat<eT> y(x.submat(10, 10, 109, 109));
  Mat<eT> orig_y(y);
  uvec cols = shuffle(linspace<uvec>(0, 99, 100));
  cols = cols.subvec(0, 50);

  // a little trickier expression here
  x.submat(10, 10, 109, 109).each_col(cols) = x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) = orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 109).each_col(cols) += x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) += orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 109).each_col(cols) -= x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) -= orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 109).each_col(cols) %= x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) %= orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 109).each_col(cols) /= x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) /= orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109) + 1, y + 1, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() on a subview with a conv_to as the RHS does the conversion implicitly.
TEMPLATE_TEST_CASE
  (
  "each2_row_subview_conv_to",
  "[each2]",
  (std::pair<double, double>), (std::pair<double, float>), (std::pair<double, u32>), (std::pair<double, s32>), (std::pair<double, u64>), (std::pair<double, s64>),
  (std::pair<float, float>), (std::pair<float, double>), (std::pair<float, u32>), (std::pair<float, s32>), (std::pair<float, u64>), (std::pair<float, s64>),
  (std::pair<u32, u32>), (std::pair<u32, double>), (std::pair<u32, float>), (std::pair<u32, s32>), (std::pair<u32, u64>), (std::pair<u32, s64>),
  (std::pair<s32, s32>), (std::pair<s32, double>), (std::pair<s32, float>), (std::pair<s32, u32>), (std::pair<s32, u64>), (std::pair<s32, s64>),
  (std::pair<u64, u64>), (std::pair<u64, double>), (std::pair<u64, float>), (std::pair<u64, u32>), (std::pair<u64, s32>), (std::pair<u64, s64>),
  (std::pair<s64, s64>), (std::pair<s64, double>), (std::pair<s64, float>), (std::pair<s64, u32>), (std::pair<s64, s32>), (std::pair<s64, u64>)
  )
  {
  typedef typename TestType::first_type eT1;
  typedef typename TestType::second_type eT2;

  if (!coot_rt_t::is_supported_type<eT1>() || !coot_rt_t::is_supported_type<eT2>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT1> x = 10 * randi<Mat<eT1>>(200, 220, distr_param(1, 50)) + 50;
  Row<eT2> v = randi<Row<eT2>>(120, distr_param(1, 50));
  Row<eT1> conv_v = conv_to<Row<eT1>>::from(v);
  uvec rows = shuffle(linspace<uvec>(0, 99, 100));
  rows = rows.subvec(0, 50);

  Mat<eT1> orig_x(x);
  Mat<eT1> y(x.submat(50, 50, 149, 169));
  Mat<eT1> orig_y(y);

  x.submat(50, 50, 149, 169).each_row(rows) = conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) = conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 149, 169).each_row(rows) += conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) += conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 149, 169).each_row(rows) -= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) -= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 149, 169).each_row(rows) %= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) %= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 149, 169).each_row(rows) /= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < rows.n_elem; ++r)
    y.row(rows[r]) /= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() with a conv_to as the RHS does the conversion implicitly.
TEMPLATE_TEST_CASE
  (
  "each2_col_subview_conv_to",
  "[each2]",
  (std::pair<double, double>), (std::pair<double, float>), (std::pair<double, u32>), (std::pair<double, s32>), (std::pair<double, u64>), (std::pair<double, s64>),
  (std::pair<float, float>), (std::pair<float, double>), (std::pair<float, u32>), (std::pair<float, s32>), (std::pair<float, u64>), (std::pair<float, s64>),
  (std::pair<u32, u32>), (std::pair<u32, double>), (std::pair<u32, float>), (std::pair<u32, s32>), (std::pair<u32, u64>), (std::pair<u32, s64>),
  (std::pair<s32, s32>), (std::pair<s32, double>), (std::pair<s32, float>), (std::pair<s32, u32>), (std::pair<s32, u64>), (std::pair<s32, s64>),
  (std::pair<u64, u64>), (std::pair<u64, double>), (std::pair<u64, float>), (std::pair<u64, u32>), (std::pair<u64, s32>), (std::pair<u64, s64>),
  (std::pair<s64, s64>), (std::pair<s64, double>), (std::pair<s64, float>), (std::pair<s64, u32>), (std::pair<s64, s32>), (std::pair<s64, u64>)
  )
  {
  typedef typename TestType::first_type eT1;
  typedef typename TestType::second_type eT2;

  if (!coot_rt_t::is_supported_type<eT1>() || !coot_rt_t::is_supported_type<eT2>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT1> x = 10 * randi<Mat<eT1>>(220, 200, distr_param(1, 50)) + 50;
  Col<eT2> v = randi<Col<eT2>>(120, distr_param(1, 50));
  Col<eT1> conv_v = conv_to<Col<eT1>>::from(v);
  uvec cols = shuffle(linspace<uvec>(0, 99, 100));
  cols = cols.subvec(0, 50);

  Mat<eT1> orig_x(x);
  Mat<eT1> y(x.submat(50, 50, 169, 149));
  Mat<eT1> orig_y(y);

  x.submat(50, 50, 169, 149).each_col(cols) = conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) = conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 169, 149).each_col(cols) += conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) += conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 169, 149).each_col(cols) -= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) -= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 169, 149).each_col(cols) %= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) %= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 169, 149).each_col(cols) /= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < cols.n_elem; ++c)
    y.col(cols[c]) /= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



//
// standalone operators for .each_row() and .each_col()
//



TEMPLATE_TEST_CASE("each2_row_standalone", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(120, distr_param(1, 50));
  Row<eT> v2 = 150 + v;
  uvec rows = linspace<uvec>(0, 99, 100);
  rows = rows.subvec(0, 50);

  Mat<eT> z = x.each_row(rows) + v;
  Mat<eT> z2(rows.n_elem, x.n_cols);
  for (uword r = 0; r < rows.n_elem; ++r)
    z2.row(r) = x.row(rows[r]);
  Mat<eT> z2_orig(z2);

  z2.each_row() += v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_row(rows) - v;
  z2 = z2_orig;
  z2.each_row() -= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  // Flip the arguments
  z = v - x.each_row(rows);
  for (uword r = 0; r < rows.n_elem; ++r)
    z2.row(r) = v - x.row(rows[r]);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_row(rows) % v;
  z2 = z2_orig;
  z2.each_row() %= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_row(rows) / v;
  z2 = z2_orig;
  z2.each_row() /= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = v2 / x.each_row(rows);
  for (uword r = 0; r < rows.n_elem; ++r)
    z2.row(r) = v2 / x.row(rows[r]);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );
  }



TEMPLATE_TEST_CASE("each2_col_standalone", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 50));
  Col<eT> v2 = 150 + v;
  uvec cols = linspace<uvec>(0, 119, 120);
  cols = cols.subvec(0, 60);

  Mat<eT> z = x.each_col(cols) + v;
  Mat<eT> z2(x.n_rows, cols.n_elem);
  for (uword c = 0; c < cols.n_elem; ++c)
    z2.col(c) = x.col(cols[c]);
  Mat<eT> z2_orig(z2);
  z2.each_col() += v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_col(cols) - v;
  z2 = z2_orig;
  z2.each_col() -= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  // Flip the arguments
  z = v - x.each_col(cols);
  for (uword c = 0; c < cols.n_elem; ++c)
    z2.col(c) = v - x.col(cols[c]);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_col(cols) % v;
  z2 = z2_orig;
  z2.each_col() %= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_col(cols) / v;
  z2 = z2_orig;
  z2.each_col() /= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = v2 / x.each_col(cols);
  for (uword c = 0; c < cols.n_elem; ++c)
    z2.col(c) = v2 / x.col(cols[c]);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );
  }



TEMPLATE_TEST_CASE("each2_row_standalone_subview", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(120, distr_param(1, 50));
  Row<eT> v2 = 150 + v;
  uvec rows = linspace<uvec>(0, 99, 100);
  rows = rows.subvec(0, 50);

  Mat<eT> z = x.submat(10, 10, 109, 129).each_row(rows) + v;
  Mat<eT> z2 = x.submat(10, 10, 109, 129);
  Mat<eT> z3(rows.n_elem, z2.n_cols);
  for (uword r = 0; r < rows.n_elem; ++r)
    z3.row(r) = z2.row(rows[r]);
  Mat<eT> z3_orig(z3);
  z3.each_row() += v;

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_row(rows) - v;
  z3 = z3_orig;
  z3.each_row() -= v;

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  // Flip the arguments
  z = v - x.submat(10, 10, 109, 129).each_row(rows);
  for (uword r = 0; r < rows.n_elem; ++r)
    z3.row(r) = v - x.submat(10 + rows[r], 10, 10 + rows[r], 129);

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_row(rows) % v;
  z3 = z3_orig;
  z3.each_row() %= v;

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_row(rows) / v;
  z3 = z3_orig;
  z3.each_row() /= v;

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  z = v2 / x.submat(10, 10, 109, 129).each_row(rows);
  for (uword r = 0; r < rows.n_elem; ++r)
    z3.row(r) = v2 / x.submat(10 + rows[r], 10, 10 + rows[r], 129);

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );
  }



TEMPLATE_TEST_CASE("each2_col_standalone_subview", "[each2]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 50));
  Col<eT> v2 = 150 + v;
  uvec cols = linspace<uvec>(0, 119, 120);
  cols = cols.subvec(0, 60);

  Mat<eT> z = x.submat(10, 10, 109, 129).each_col(cols) + v;
  Mat<eT> z2 = x.submat(10, 10, 109, 129);
  Mat<eT> z3(z2.n_rows, cols.n_elem);
  for (uword c = 0; c < cols.n_elem; ++c)
    z3.col(c) = z2.col(cols[c]);
  Mat<eT> z3_orig(z3);
  z3.each_col() += v;

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_col(cols) - v;
  z3 = z3_orig;
  z3.each_col() -= v;

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  // Flip the arguments
  z = v - x.submat(10, 10, 109, 129).each_col(cols);
  for (uword c = 0; c < cols.n_elem; ++c)
    z3.col(c) = v - x.submat(10, 10 + cols[c], 109, 10 + cols[c]);

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_col(cols) % v;
  z3 = z3_orig;
  z3.each_col() %= v;

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_col(cols) / v;
  z3 = z3_orig;
  z3.each_col() /= v;

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );

  z = v2 / x.submat(10, 10, 109, 129).each_col(cols);
  for (uword c = 0; c < cols.n_elem; ++c)
    z3.col(c) = v2 / x.submat(10, 10 + cols[c], 109, 10 + cols[c]);

  REQUIRE( approx_equal(z, z3, "both", 1e-5, 1e-5) );
  }
