// Copyright 2024 Ryan Curtin (http://www.ratml.org)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <armadillo>
#include <bandicoot>
#include "catch.hpp"

using namespace coot;

TEMPLATE_TEST_CASE("logspace_1", "[logspace]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> a = logspace<Col<eT>>(1,5,5);

  REQUIRE(eT(a(0)) == Approx(eT(std::pow(10.0, 1.0))));
  REQUIRE(eT(a(1)) == Approx(eT(std::pow(10.0, 2.0))));
  REQUIRE(eT(a(2)) == Approx(eT(std::pow(10.0, 3.0))));
  REQUIRE(eT(a(3)) == Approx(eT(std::pow(10.0, 4.0))));
  REQUIRE(eT(a(4)) == Approx(eT(std::pow(10.0, 5.0))));

  Col<eT> b = logspace<Col<eT>>(1,5,6);

  REQUIRE(eT(b(0)) == Approx(eT(std::pow(10.0, 1.0))));
  REQUIRE(eT(b(1)) == Approx(eT(std::pow(10.0, 1.8))));
  REQUIRE(eT(b(2)) == Approx(eT(std::pow(10.0, 2.6))));
  REQUIRE(eT(b(3)) == Approx(eT(std::pow(10.0, 3.4))));
  REQUIRE(eT(b(4)) == Approx(eT(std::pow(10.0, 4.2))));
  REQUIRE(eT(b(5)) == Approx(eT(std::pow(10.0, 5.0))));

  Row<eT> c = logspace<Row<eT>>(1,5,6);

  REQUIRE(eT(c(0)) == Approx(eT(std::pow(10.0, 1.0))));
  REQUIRE(eT(c(1)) == Approx(eT(std::pow(10.0, 1.8))));
  REQUIRE(eT(c(2)) == Approx(eT(std::pow(10.0, 2.6))));
  REQUIRE(eT(c(3)) == Approx(eT(std::pow(10.0, 3.4))));
  REQUIRE(eT(c(4)) == Approx(eT(std::pow(10.0, 4.2))));
  REQUIRE(eT(c(5)) == Approx(eT(std::pow(10.0, 5.0))));

  Mat<eT> X = logspace<Mat<eT>>(1,5,6);

  REQUIRE(X.n_rows == 6);
  REQUIRE(X.n_cols == 1);
  }



TEMPLATE_TEST_CASE("logspace_2", "[logspace]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> a = logspace<Col<eT>>(1,5,5);

  // Allow margin of 1 because integers may truncate.
  REQUIRE(eT(a(0)) == Approx(eT(std::pow(10.0, 1.0))).margin(1.0));
  REQUIRE(eT(a(1)) == Approx(eT(std::pow(10.0, 2.0))).margin(1.0));
  REQUIRE(eT(a(2)) == Approx(eT(std::pow(10.0, 3.0))).margin(1.0));
  REQUIRE(eT(a(3)) == Approx(eT(std::pow(10.0, 4.0))).margin(1.0));
  REQUIRE(eT(a(4)) == Approx(eT(std::pow(10.0, 5.0))).margin(1.0));

  Mat<eT> X = logspace<Mat<eT>>(1,5,6);

  REQUIRE(X.n_rows == 6);
  REQUIRE(X.n_cols == 1);
  }



TEMPLATE_TEST_CASE("logspace_arma_comparison", "[logspace]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = logspace<Mat<eT>>(1, 3, 1000);
  arma::Mat<eT> x_ref = arma::logspace<arma::Mat<eT>>(1, 3, 1000);

  arma::Mat<eT> x_cpu(x);

  REQUIRE( arma::approx_equal(x_ref, x_cpu, "both", 1e-5, 1e-5) );
  }
