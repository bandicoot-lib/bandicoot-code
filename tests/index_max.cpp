// Copyright 2019 Ryan Curtin (http://www.ratml.org/)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <armadillo>
#include <bandicoot>
#include "catch.hpp"

using namespace coot;

TEMPLATE_TEST_CASE("index_max_small", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> x(16);
  for (uword i = 0; i < 16; ++i)
    x[i] = i + 1;

  uword max_index = index_max(x);

  REQUIRE(max_index == uword(15));
  }



TEMPLATE_TEST_CASE("index_max_1", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> x(6400);
  for (uword i = 0; i < 6400; ++i)
    x[i] = i + 1;

  uword max_index = index_max(x);

  REQUIRE(max_index == uword(6399));
  }



TEMPLATE_TEST_CASE("index_max_strange_size", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> x(608);

  for(uword i = 0; i < 608; ++i)
    x[i] = i + 1;
  x[371] = eT(1000);

  uword max_index = index_max(x);

  REQUIRE(max_index == 371);
  }



TEMPLATE_TEST_CASE("index_max_large", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  arma::Col<eT> cpu_x = arma::conv_to<arma::Col<eT>>::from(arma::randu<arma::Col<double>>(100000) * 10.0);
  cpu_x.randu();
  Col<eT> x(cpu_x);

  uword cpu_max_index = index_max(cpu_x);
  uword max_index = index_max(x);

  REQUIRE(max_index == cpu_max_index);
  }


TEMPLATE_TEST_CASE("index_max_2", "[index_max]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> x(50);
  x.randu();
  x += eT(1);

  uword max_index = index_max(x);

  REQUIRE( max_index >= uword(0) );
  REQUIRE( max_index < uword(50) );
  }



TEMPLATE_TEST_CASE("index_max_3", "[index_max]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 1);
  x[0] = 4.7394;
  x[1] = 4.7299;
  x[2] = 6.2287;
  x[3] = 4.5893;
  x[4] = 4.4460;
  x[5] = 4.7376;
  x[6] = 4.7432;
  x[7] = 3.8152;
  x[8] = 4.1442;
  x[9] = 5.2339;

  uword max_index = index_max(vectorise(x));

  REQUIRE( max_index == 2 );
  }



TEMPLATE_TEST_CASE("index_max_colwise_1", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 100 + c;
        }
      else
        {
        x(r, c) = c;
        }
      }
    }

  Mat<uword> s = index_max(x, 0);

  REQUIRE( s.n_rows == 1  );
  REQUIRE( s.n_cols == 10 );
  for (uword c = 0; c < 10; ++c)
    {
    REQUIRE( uword(s[c]) == c );
    }
  }



TEMPLATE_TEST_CASE("index_max_colwise_2", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      x(r, c) = r;
      }
    }

  Mat<uword> s = index_max(x, 0);

  REQUIRE( s.n_rows == 1  );
  REQUIRE( s.n_cols == 10 );
  for (uword c = 0; c < 10; ++c)
    {
    REQUIRE( uword(s[c]) == 9 );
    }
  }



TEMPLATE_TEST_CASE("index_max_rowwise_1", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      x(r, c) = c;
      }
    }

  Mat<uword> s = index_max(x, 1);

  REQUIRE( s.n_rows == 10 );
  REQUIRE( s.n_cols == 1  );
  for (uword r = 0; r < 10; ++r)
    {
    REQUIRE( uword(s[r]) == 9 );
    }
  }



TEMPLATE_TEST_CASE("index_max_rowwise_2", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 2 * r;
        }
      else
        {
        x(r, c) = r;
        }
      }
    }

  Mat<uword> s = index_max(x, 1);

  REQUIRE( s.n_rows == 10 );
  REQUIRE( s.n_cols == 1  );
  for (uword r = 0; r < 10; ++r)
    {
    REQUIRE( uword(s[r]) == r );
    }
  }



TEMPLATE_TEST_CASE("subview_index_max_colwise_1", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 100 + c;
        }
      else
        {
        x(r, c) = c;
        }
      }
    }

  Mat<uword> s = index_max(x.submat(1, 1, 8, 8), 0);

  REQUIRE( s.n_rows == 1 );
  REQUIRE( s.n_cols == 8 );
  for (uword c = 0; c < 8; ++c)
    {
    REQUIRE( uword(s[c]) == c );
    }
  }



TEMPLATE_TEST_CASE("subview_index_max_colwise_2", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      x(r, c) = r;
      }
    }

  Mat<uword> s = index_max(x.submat(1, 1, 8, 8), 0);

  REQUIRE( s.n_rows == 1 );
  REQUIRE( s.n_cols == 8 );
  for (uword c = 0; c < 8; ++c)
    {
    REQUIRE( uword(s[c]) == 7 );
    }
  }



TEMPLATE_TEST_CASE("subview_index_max_colwise_full", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = c + 100;
        }
      else
        {
        x(r, c) = c;
        }
      }
    }

  Mat<uword> s = index_max(x.submat(0, 0, 9, 9), 0);

  REQUIRE( s.n_rows == 1  );
  REQUIRE( s.n_cols == 10 );
  for (uword c = 0; c < 10; ++c)
    {
    REQUIRE( uword(s[c]) == c );
    }
  }



TEMPLATE_TEST_CASE("subview_index_max_rowwise_1", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      x(r, c) = c;
      }
    }

  Mat<uword> s = index_max(x.submat(1, 1, 8, 8), 1);

  REQUIRE( s.n_rows == 8 );
  REQUIRE( s.n_cols == 1 );
  for (uword r = 0; r < 8; ++r)
    {
    REQUIRE( uword(s[r]) == 7 );
    }
  }



TEMPLATE_TEST_CASE("subview_index_max_rowwise_2", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = r + 100;
        }
      else
        {
        x(r, c) = r;
        }
      }
    }

  Mat<uword> s = index_max(x.submat(1, 1, 8, 8), 1);

  REQUIRE( s.n_rows == 8 );
  REQUIRE( s.n_cols == 1 );
  for (uword r = 0; r < 8; ++r)
    {
    REQUIRE( uword(s[r]) == r );
    }
  }



TEMPLATE_TEST_CASE("subview_index_max_rowwise_full", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = r + 100;
        }
      else
        {
        x(r, c) = r;
        }
      }
    }

  Mat<uword> s = index_max(x.submat(0, 0, 9, 9), 1);

  REQUIRE( s.n_rows == 10 );
  REQUIRE( s.n_cols == 1  );
  for (uword r = 0; r < 10; ++r)
    {
    REQUIRE( uword(s[r]) == r );
    }
  }



TEST_CASE("alias_index_max_colwise", "[index_max]")
  {
  Mat<uword> x = randi<Mat<uword>>(100, 100, distr_param(0, 100000));
  Mat<uword> y(x);

  Mat<uword> r1 = index_max(y, 0);
  x = index_max(x, 0);

  REQUIRE( x.n_rows == r1.n_rows );
  REQUIRE( x.n_cols == r1.n_cols );

  for (uword c = 0; c < r1.n_cols; ++c)
    {
    for (uword r = 0; r < r1.n_rows; ++r)
      {
      REQUIRE( uword(x(r, c)) == uword(r1(r, c)) );
      }
    }
  }



TEST_CASE("alias_index_max_rowwise", "[index_max]")
  {
  Mat<uword> x = randi<Mat<uword>>(100, 100, distr_param(0, 100000));
  Mat<uword> y(x);

  Mat<uword> r1 = index_max(y, 1);
  x = index_max(x, 1);

  REQUIRE( x.n_rows == r1.n_rows );
  REQUIRE( x.n_cols == r1.n_cols );

  for (uword c = 0; c < r1.n_cols; ++c)
    {
    for (uword r = 0; r < r1.n_rows; ++r)
      {
      REQUIRE( uword(x(r, c)) == uword(r1(r, c)) );
      }
    }
  }



TEMPLATE_TEST_CASE("member_index_max", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(100, 100, fill::randu);

  uword index_max1 = index_max(vectorise(x));
  uword index_max2 = x.index_max();

  REQUIRE( index_max1 == index_max2 );

  Col<eT> y(100, fill::randu);

  index_max1 = index_max(y);
  index_max2 = y.index_max();

  REQUIRE( index_max1 == index_max2 );

  Row<eT> z(100, fill::randu);

  index_max1 = index_max(z);
  index_max2 = z.index_max();

  REQUIRE( index_max1 == index_max2 );
  }



TEMPLATE_TEST_CASE("member_max_with_index", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(60, 100, fill::randu);

  uword index1;
  uword index2;
  uword row_index;
  uword col_index;

  index1 = index_max(vectorise(x));
  const eT max_val1 = x[index1];

  const eT max_val2 = x.max(index2);

  const eT max_val3 = x.max(row_index, col_index);
  const uword index3 = row_index + col_index * x.n_rows;

  REQUIRE( max_val1 == max_val2 );
  REQUIRE( max_val1 == max_val3 );
  REQUIRE( index1 == index2 );
  REQUIRE( index1 == index3 );
  }



TEMPLATE_TEST_CASE("expr_member_index_max", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(100, 50, fill::randu);
  Mat<eT> y(50, 100, fill::randu);

  Mat<eT> z = (3.0 + (x % y.t()));

  uword index_max1 = (3.0 + (x % y.t())).index_max();
  uword index_max2 = z.index_max();

  REQUIRE( index_max1 == index_max2 );
  }



TEMPLATE_TEST_CASE("expr_member_max_with_index", "[index_max]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(100, 50, fill::randu);
  Mat<eT> y(50, 100, fill::randu);

  Mat<eT> z = (3.0 + (x % y.t()));

  uword index1;
  uword index2;
  uword row_index;
  uword col_index;

  index1 = index_max(vectorise(z));
  const eT max_val1 = z[index1];

  const eT max_val2 = (3.0 + (x % y.t())).max(index2);

  const eT max_val3 = (3.0 + (x % y.t())).max(row_index, col_index);
  const uword index3 = row_index + col_index * x.n_rows;

  REQUIRE( max_val1 == max_val2 );
  REQUIRE( max_val1 == max_val3 );
  REQUIRE( index1 == index2 );
  REQUIRE( index1 == index3 );
  }
