// Copyright 2025 Ryan Curtin (http://www.ratml.org/)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <bandicoot>
#include "catch.hpp"

using namespace coot;

// Make sure .each_row() on an empty matrix does nothing.
TEMPLATE_TEST_CASE("each_row_empty", "[each]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // size is 0x0
  Mat<eT> x;

  x.each_row() = zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_row() += zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_row() -= zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_row() %= zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_row() /= zeros<Mat<eT>>(1, x.n_cols);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );
  }



// Make sure .each_col() on an empty matrix does nothing.
TEMPLATE_TEST_CASE("each_col_empty", "[each]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x;

  x.each_col() = zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_col() += zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_col() -= zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_col() %= zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );

  x.each_col() /= zeros<Mat<eT>>(x.n_rows, 1);

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );
  }



// Make sure that .each_row() on a one-row matrix is the same as just modifying the matrix.
TEMPLATE_TEST_CASE("each_row_one_row", "[each]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(1, 100, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 50));
  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_row() = v;
  y = v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() += v;
  y += v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() -= v;
  y -= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() %= v;
  y %= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() /= v;
  y /= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() on a one-col matrix is the same as just modifying the matrix.
TEMPLATE_TEST_CASE("each_col_one_col", "[each]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(50, 1, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(50, distr_param(1, 50));
  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_col() = v;
  y = v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() += v;
  y += v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() -= v;
  y -= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() %= v;
  y %= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() /= v;
  y /= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() works correctly on a full matrix.
TEMPLATE_TEST_CASE("each_row_full_mat", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(120, 100, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 10));

  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_row() = v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) = v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() += v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) += v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() -= v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) -= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() %= v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) %= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() /= v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) /= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() works correctly on a full matrix.
TEMPLATE_TEST_CASE("each_col_full_mat", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 10));

  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_col() = v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) = v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() += v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) += v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() -= v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) -= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() %= v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) %= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() /= v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) /= v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() works correctly on an expression.
TEMPLATE_TEST_CASE("each_row_full_mat_expr", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(120, 100, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 10));

  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_row() = (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) = (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() += (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) += (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() -= (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) -= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() %= (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) %= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() /= (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) /= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() works correctly on an expression.
TEMPLATE_TEST_CASE("each_col_full_mat_expr", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 10));

  Mat<eT> orig_x(x);
  Mat<eT> y = x;

  x.each_col() = (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) = (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() += (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) += (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() -= (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) -= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() %= (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) %= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() /= (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) /= (2 * v.t() + 3);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() on an alias works properly.
TEMPLATE_TEST_CASE("each_row_full_mat_alias", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  x.row(0) = randi<Row<eT>>(120, distr_param(1, 20));
  Mat<eT> orig_x(x);
  Mat<eT> y(x);

  x.each_row() = x.row(0);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) = orig_x.row(0);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() += x.row(0);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) += orig_x.row(0);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() -= x.row(0);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) -= orig_x.row(0);

  // Add 1 to each element to avoid 0s in the matrices being compared
  REQUIRE( approx_equal(x + 1, y + 1, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() %= x.row(0);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) %= orig_x.row(0);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() /= x.row(0);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) /= orig_x.row(0);

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() on an alias works properly.
TEMPLATE_TEST_CASE("each_col_full_mat_alias", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(10, 10, distr_param(1, 50)) + 100;
  x.row(0) = randi<Row<eT>>(10, distr_param(1, 20)) + 20;
  x(0, 0) = 5;
  Mat<eT> orig_x(x);
  Mat<eT> y(x);

  // a little trickier expression here
  x.each_col() = x.row(0).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) = orig_x.row(0).t();

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() += x.row(0).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) += orig_x.row(0).t();

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() -= x.row(0).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) -= orig_x.row(0).t();

  // Avoid zeros in the matrices being compared
  REQUIRE( approx_equal(x + 1, y + 1, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() %= x.row(0).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) %= orig_x.row(0).t();

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() /= x.row(0).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) /= orig_x.row(0).t();

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() with a conv_to as the RHS does the conversion implicitly.
TEMPLATE_TEST_CASE
  (
  "each_row_conv_to",
  "[each]",
  (std::pair<double, double>), (std::pair<double, float>), (std::pair<double, u32>), (std::pair<double, s32>), (std::pair<double, u64>), (std::pair<double, s64>),
  (std::pair<float, float>), (std::pair<float, double>), (std::pair<float, u32>), (std::pair<float, s32>), (std::pair<float, u64>), (std::pair<float, s64>),
  (std::pair<u32, u32>), (std::pair<u32, double>), (std::pair<u32, float>), (std::pair<u32, s32>), (std::pair<u32, u64>), (std::pair<u32, s64>),
  (std::pair<s32, s32>), (std::pair<s32, double>), (std::pair<s32, float>), (std::pair<s32, u32>), (std::pair<s32, u64>), (std::pair<s32, s64>),
  (std::pair<u64, u64>), (std::pair<u64, double>), (std::pair<u64, float>), (std::pair<u64, u32>), (std::pair<u64, s32>), (std::pair<u64, s64>),
  (std::pair<s64, s64>), (std::pair<s64, double>), (std::pair<s64, float>), (std::pair<s64, u32>), (std::pair<s64, s32>), (std::pair<s64, u64>)
  )
  {
  typedef typename TestType::first_type eT1;
  typedef typename TestType::second_type eT2;

  if (!coot_rt_t::is_supported_type<eT1>() || !coot_rt_t::is_supported_type<eT2>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT1> x = 10 * randi<Mat<eT1>>(100, 120, distr_param(1, 50)) + 50;
  Row<eT2> v = randi<Row<eT2>>(120, distr_param(1, 50));
  Row<eT1> conv_v = conv_to<Row<eT1>>::from(v);

  Mat<eT1> orig_x(x);
  Mat<eT1> y(x);

  x.each_row() = conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) = conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() += conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) += conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() -= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) -= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() %= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) %= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_row() /= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) /= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() with a conv_to as the RHS does the conversion implicitly.
TEMPLATE_TEST_CASE
  (
  "each_col_conv_to",
  "[each]",
  (std::pair<double, double>), (std::pair<double, float>), (std::pair<double, u32>), (std::pair<double, s32>), (std::pair<double, u64>), (std::pair<double, s64>),
  (std::pair<float, float>), (std::pair<float, double>), (std::pair<float, u32>), (std::pair<float, s32>), (std::pair<float, u64>), (std::pair<float, s64>),
  (std::pair<u32, u32>), (std::pair<u32, double>), (std::pair<u32, float>), (std::pair<u32, s32>), (std::pair<u32, u64>), (std::pair<u32, s64>),
  (std::pair<s32, s32>), (std::pair<s32, double>), (std::pair<s32, float>), (std::pair<s32, u32>), (std::pair<s32, u64>), (std::pair<s32, s64>),
  (std::pair<u64, u64>), (std::pair<u64, double>), (std::pair<u64, float>), (std::pair<u64, u32>), (std::pair<u64, s32>), (std::pair<u64, s64>),
  (std::pair<s64, s64>), (std::pair<s64, double>), (std::pair<s64, float>), (std::pair<s64, u32>), (std::pair<s64, s32>), (std::pair<s64, u64>)
  )
  {
  typedef typename TestType::first_type eT1;
  typedef typename TestType::second_type eT2;

  if (!coot_rt_t::is_supported_type<eT1>() || !coot_rt_t::is_supported_type<eT2>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT1> x = 10 * randi<Mat<eT1>>(120, 100, distr_param(1, 50)) + 50;
  Col<eT2> v = randi<Col<eT2>>(120, distr_param(1, 50));
  Col<eT1> conv_v = conv_to<Col<eT1>>::from(v);

  Mat<eT1> orig_x(x);
  Mat<eT1> y(x);

  x.each_col() = conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) = conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() += conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) += conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() -= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) -= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() %= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) %= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );

  x = orig_x;
  y = orig_x;

  x.each_col() /= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) /= conv_v;

  REQUIRE( approx_equal(x, y, "both", 1e-5, 1e-5) );
  }



//
// subviews
//



// Make sure that .each_row() on a one-row subview is the same as just modifying the matrix.
TEMPLATE_TEST_CASE("each_row_subview_one_row", "[each]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(3, 150, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 50));
  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(0, 0, 0, 99);
  Mat<eT> orig_y(y);

  x.submat(0, 0, 0, 99).each_row() = v;
  y = v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 0, 99).each_row() += v;
  y += v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 0, 99).each_row() -= v;
  y -= v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 0, 99).each_row() %= v;
  y %= v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 0, 99).each_row() /= v;
  y /= v;

  REQUIRE( approx_equal(x.submat(0, 0, 0, 99), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 0, 99) = orig_x.submat(0, 0, 0, 99);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() on a one-col subview is the same as just modifying the matrix.
TEMPLATE_TEST_CASE("each_col_subview_one_col", "[each]", double, float, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(150, 3, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 50));
  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(0, 1, 99, 1);
  Mat<eT> orig_y(y);

  x.submat(0, 1, 99, 1).each_col() = v;
  y = v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 1, 99, 1).each_col() += v;
  y += v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 1, 99, 1).each_col() -= v;
  y -= v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 1, 99, 1).each_col() %= v;
  y %= v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 1, 99, 1).each_col() /= v;
  y /= v;

  REQUIRE( approx_equal(x.submat(0, 1, 99, 1), y, "both", 1e-5, 1e-5) );
  x.submat(0, 1, 99, 1) = orig_x.submat(0, 1, 99, 1);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() on a subview works correctly for more than one row.
TEMPLATE_TEST_CASE("each_row_subview_full_mat", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(220, 200, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 10));

  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(10, 10, 129, 109);
  Mat<eT> orig_y(y);

  x.submat(10, 10, 129, 109).each_row() = v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) = v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 129, 109).each_row() += v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) += v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 129, 109).each_row() -= v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) -= v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 129, 109).each_row() %= v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) %= v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 129, 109).each_row() /= v;
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) /= v;

  REQUIRE( approx_equal(x.submat(10, 10, 129, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 129, 109) = orig_x.submat(10, 10, 129, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() on a subview works correctly for more than one row.
TEMPLATE_TEST_CASE("each_col_subview_full_mat", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 10));

  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(15, 15, 114, 134);
  Mat<eT> orig_y(y);

  x.submat(15, 15, 114, 134).each_col() = v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) = v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 114, 134).each_col() += v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) += v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 114, 134).each_col() -= v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) -= v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 114, 134).each_col() %= v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) %= v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 114, 134).each_col() /= v;
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) /= v;

  REQUIRE( approx_equal(x.submat(15, 15, 114, 134), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 114, 134) = orig_x.submat(15, 15, 114, 134);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() on a subview works correctly on an expression.
TEMPLATE_TEST_CASE("each_row_subview_full_mat_expr", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(220, 200, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 10));

  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(15, 15, 134, 114);
  Mat<eT> orig_y(y);

  x.submat(15, 15, 134, 114).each_row() = (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) = (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 134, 114).each_row() += (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) += (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 134, 114).each_row() -= (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) -= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 134, 114).each_row() %= (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) %= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(15, 15, 134, 114).each_row() /= (2 * v.t() + 3);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) /= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(15, 15, 134, 114), y, "both", 1e-5, 1e-5) );
  x.submat(15, 15, 134, 114) = orig_x.submat(15, 15, 134, 114);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() on a subview works correctly on an expression.
TEMPLATE_TEST_CASE("each_col_subview_full_mat_expr", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(100, distr_param(1, 10));

  Mat<eT> orig_x(x);
  Mat<eT> y = x.submat(10, 10, 109, 129);
  Mat<eT> orig_y(y);

  x.submat(10, 10, 109, 129).each_col() = (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) = (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 129).each_col() += (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) += (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 129).each_col() -= (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) -= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 129).each_col() %= (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) %= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 129).each_col() /= (2 * v.t() + 3);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) /= (2 * v.t() + 3);

  REQUIRE( approx_equal(x.submat(10, 10, 109, 129), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 129) = orig_x.submat(10, 10, 109, 129);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() on a subview with an alias as input works properly.
TEMPLATE_TEST_CASE("each_row_subview_full_mat_alias", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  x.submat(0, 10, 0, 129).fill(2);
  Mat<eT> orig_x(x);
  Mat<eT> y(x.submat(0, 0, 99, 119));
  Mat<eT> orig_y(y);

  x.submat(0, 0, 99, 119).each_row() = x.submat(0, 10, 0, 129);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) = orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 99, 119).each_row() += x.submat(0, 10, 0, 129);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) += orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 99, 119).each_row() -= x.submat(0, 10, 0, 129);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) -= orig_x.submat(0, 10, 0, 129);

  // The +1 is to avoid zeroes.
  REQUIRE( approx_equal(x.submat(0, 0, 99, 119) + 1, y + 1, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 99, 119).each_row() %= x.submat(0, 10, 0, 129);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) %= orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(0, 0, 99, 119).each_row() /= x.submat(0, 10, 0, 129);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) /= orig_x.submat(0, 10, 0, 129);

  REQUIRE( approx_equal(x.submat(0, 0, 99, 119), y, "both", 1e-5, 1e-5) );
  x.submat(0, 0, 99, 119) = orig_x.submat(0, 0, 99, 119);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() on a subview with an alias as input works properly.
TEMPLATE_TEST_CASE("each_col_subview_full_mat_alias", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT> x = 10 * randi<Mat<eT>>(200, 200, distr_param(1, 50)) + 50;
  x.submat(0, 0, 0, 99) = randi<Mat<eT>>(1, 100, distr_param(1, 10));
  x(0, 0) = 2;
  Mat<eT> orig_x(x);
  Mat<eT> y(x.submat(10, 10, 109, 109));
  Mat<eT> orig_y(y);

  // a little trickier expression here
  x.submat(10, 10, 109, 109).each_col() = x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) = orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 109).each_col() += x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) += orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 109).each_col() -= x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) -= orig_x.submat(0, 0, 0, 99).t();

  // The +1 is to avoid zero values.
  REQUIRE( approx_equal(x.submat(10, 10, 109, 109) + 1, y + 1, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 109).each_col() %= x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) %= orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(10, 10, 109, 109).each_col() /= x.submat(0, 0, 0, 99).t();
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) /= orig_x.submat(0, 0, 0, 99).t();

  REQUIRE( approx_equal(x.submat(10, 10, 109, 109), y, "both", 1e-5, 1e-5) );
  x.submat(10, 10, 109, 109) = orig_x.submat(10, 10, 109, 109);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_row() on a subview with a conv_to as the RHS does the conversion implicitly.
TEMPLATE_TEST_CASE
  (
  "each_row_subview_conv_to",
  "[each]",
  (std::pair<double, double>), (std::pair<double, float>), (std::pair<double, u32>), (std::pair<double, s32>), (std::pair<double, u64>), (std::pair<double, s64>),
  (std::pair<float, float>), (std::pair<float, double>), (std::pair<float, u32>), (std::pair<float, s32>), (std::pair<float, u64>), (std::pair<float, s64>),
  (std::pair<u32, u32>), (std::pair<u32, double>), (std::pair<u32, float>), (std::pair<u32, s32>), (std::pair<u32, u64>), (std::pair<u32, s64>),
  (std::pair<s32, s32>), (std::pair<s32, double>), (std::pair<s32, float>), (std::pair<s32, u32>), (std::pair<s32, u64>), (std::pair<s32, s64>),
  (std::pair<u64, u64>), (std::pair<u64, double>), (std::pair<u64, float>), (std::pair<u64, u32>), (std::pair<u64, s32>), (std::pair<u64, s64>),
  (std::pair<s64, s64>), (std::pair<s64, double>), (std::pair<s64, float>), (std::pair<s64, u32>), (std::pair<s64, s32>), (std::pair<s64, u64>)
  )
  {
  typedef typename TestType::first_type eT1;
  typedef typename TestType::second_type eT2;

  if (!coot_rt_t::is_supported_type<eT1>() || !coot_rt_t::is_supported_type<eT2>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT1> x = 10 * randi<Mat<eT1>>(200, 220, distr_param(1, 50)) + 50;
  Row<eT2> v = randi<Row<eT2>>(120, distr_param(1, 50));
  Row<eT1> conv_v = conv_to<Row<eT1>>::from(v);

  Mat<eT1> orig_x(x);
  Mat<eT1> y(x.submat(50, 50, 149, 169));
  Mat<eT1> orig_y(y);

  x.submat(50, 50, 149, 169).each_row() = conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) = conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 149, 169).each_row() += conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) += conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 149, 169).each_row() -= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) -= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 149, 169).each_row() %= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) %= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 149, 169).each_row() /= conv_to<Row<eT1>>::from(v);
  for (uword r = 0; r < y.n_rows; ++r)
    y.row(r) /= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 149, 169), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 149, 169) = orig_x.submat(50, 50, 149, 169);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



// Make sure that .each_col() with a conv_to as the RHS does the conversion implicitly.
TEMPLATE_TEST_CASE
  (
  "each_col_subview_conv_to",
  "[each]",
  (std::pair<double, double>), (std::pair<double, float>), (std::pair<double, u32>), (std::pair<double, s32>), (std::pair<double, u64>), (std::pair<double, s64>),
  (std::pair<float, float>), (std::pair<float, double>), (std::pair<float, u32>), (std::pair<float, s32>), (std::pair<float, u64>), (std::pair<float, s64>),
  (std::pair<u32, u32>), (std::pair<u32, double>), (std::pair<u32, float>), (std::pair<u32, s32>), (std::pair<u32, u64>), (std::pair<u32, s64>),
  (std::pair<s32, s32>), (std::pair<s32, double>), (std::pair<s32, float>), (std::pair<s32, u32>), (std::pair<s32, u64>), (std::pair<s32, s64>),
  (std::pair<u64, u64>), (std::pair<u64, double>), (std::pair<u64, float>), (std::pair<u64, u32>), (std::pair<u64, s32>), (std::pair<u64, s64>),
  (std::pair<s64, s64>), (std::pair<s64, double>), (std::pair<s64, float>), (std::pair<s64, u32>), (std::pair<s64, s32>), (std::pair<s64, u64>)
  )
  {
  typedef typename TestType::first_type eT1;
  typedef typename TestType::second_type eT2;

  if (!coot_rt_t::is_supported_type<eT1>() || !coot_rt_t::is_supported_type<eT2>())
    {
    return;
    }

  // Values chosen to avoid underflows and overflows.
  Mat<eT1> x = 10 * randi<Mat<eT1>>(220, 200, distr_param(1, 50)) + 50;
  Col<eT2> v = randi<Col<eT2>>(120, distr_param(1, 50));
  Col<eT1> conv_v = conv_to<Col<eT1>>::from(v);

  Mat<eT1> orig_x(x);
  Mat<eT1> y(x.submat(50, 50, 169, 149));
  Mat<eT1> orig_y(y);

  x.submat(50, 50, 169, 149).each_col() = conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) = conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 169, 149).each_col() += conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) += conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 169, 149).each_col() -= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) -= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 169, 149).each_col() %= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) %= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );

  y = orig_y;

  x.submat(50, 50, 169, 149).each_col() /= conv_to<Col<eT1>>::from(v);
  for (uword c = 0; c < y.n_cols; ++c)
    y.col(c) /= conv_v;

  REQUIRE( approx_equal(x.submat(50, 50, 169, 149), y, "both", 1e-5, 1e-5) );
  x.submat(50, 50, 169, 149) = orig_x.submat(50, 50, 169, 149);
  REQUIRE( approx_equal(x, orig_x, "both", 1e-5, 1e-5) );
  }



//
// standalone operators for .each_row() and .each_col()
//



TEMPLATE_TEST_CASE("each_row_standalone", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(120, distr_param(1, 50));
  Row<eT> v2 = 150 + v;

  Mat<eT> z = x.each_row() + v;
  Mat<eT> z2 = x;
  z2.each_row() += v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_row() - v;
  z2 = x;
  z2.each_row() -= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  // Flip the arguments
  z = v - x.each_row();
  for (uword r = 0; r < z2.n_rows; ++r)
    z2.row(r) = v - x.row(r);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_row() % v;
  z2 = x;
  z2.each_row() %= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_row() / v;
  z2 = x;
  z2.each_row() /= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = v2 / x.each_row();
  for (uword r = 0; r < z2.n_rows; ++r)
    z2.row(r) = v2 / x.row(r);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );
  }



TEMPLATE_TEST_CASE("each_col_standalone", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(100, 120, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 50));
  Col<eT> v2 = 150 + v;

  Mat<eT> z = x.each_col() + v;
  Mat<eT> z2 = x;
  z2.each_col() += v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_col() - v;
  z2 = x;
  z2.each_col() -= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  // Flip the arguments
  z = v - x.each_col();
  for (uword c = 0; c < z2.n_cols; ++c)
    z2.col(c) = v - x.col(c);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_col() % v;
  z2 = x;
  z2.each_col() %= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.each_col() / v;
  z2 = x;
  z2.each_col() /= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = v2 / x.each_col();
  for (uword c = 0; c < z2.n_cols; ++c)
    z2.col(c) = v2 / x.col(c);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );
  }



TEMPLATE_TEST_CASE("each_row_standalone_subview", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  Row<eT> v = randi<Row<eT>>(120, distr_param(1, 50));
  Row<eT> v2 = 150 + v;

  Mat<eT> z = x.submat(10, 10, 109, 129).each_row() + v;
  Mat<eT> z2 = x.submat(10, 10, 109, 129);
  z2.each_row() += v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_row() - v;
  z2 = x.submat(10, 10, 109, 129);
  z2.each_row() -= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  // Flip the arguments
  z = v - x.submat(10, 10, 109, 129).each_row();
  for (uword r = 0; r < z2.n_rows; ++r)
    z2.row(r) = v - x.submat(10 + r, 10, 10 + r, 129);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_row() % v;
  z2 = x.submat(10, 10, 109, 129);
  z2.each_row() %= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_row() / v;
  z2 = x.submat(10, 10, 109, 129);
  z2.each_row() /= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = v2 / x.submat(10, 10, 109, 129).each_row();
  for (uword r = 0; r < z2.n_rows; ++r)
    z2.row(r) = v2 / x.submat(10 + r, 10, 10 + r, 129);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );
  }



TEMPLATE_TEST_CASE("each_col_standalone_subview", "[each]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(200, 220, distr_param(1, 50)) + 50;
  Col<eT> v = randi<Col<eT>>(100, distr_param(1, 50));
  Col<eT> v2 = 150 + v;

  Mat<eT> z = x.submat(10, 10, 109, 129).each_col() + v;
  Mat<eT> z2 = x.submat(10, 10, 109, 129);
  z2.each_col() += v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_col() - v;
  z2 = x.submat(10, 10, 109, 129);
  z2.each_col() -= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  // Flip the arguments
  z = v - x.submat(10, 10, 109, 129).each_col();
  for (uword c = 0; c < z2.n_cols; ++c)
    z2.col(c) = v - x.submat(10, 10 + c, 109, 10 + c);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_col() % v;
  z2 = x.submat(10, 10, 109, 129);
  z2.each_col() %= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = x.submat(10, 10, 109, 129).each_col() / v;
  z2 = x.submat(10, 10, 109, 129);
  z2.each_col() /= v;

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );

  z = v2 / x.submat(10, 10, 109, 129).each_col();
  for (uword c = 0; c < z2.n_cols; ++c)
    z2.col(c) = v2 / x.submat(10, 10 + c, 109, 10 + c);

  REQUIRE( approx_equal(z, z2, "both", 1e-5, 1e-5) );
  }
